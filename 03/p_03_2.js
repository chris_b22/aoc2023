const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

let rows = data.split( '\r\n' );

const numberPositions = []
const symbolPositionsList = []

for ( const [ i, row ] of rows.entries() ) {
    const symbols = [ ...row.matchAll( /\*/g ) ]
    if ( symbols.length === 0 ) {
        symbolPositionsList.push( [] )
    } else {
        symbolPositionsList.push( symbols.map( e => e.index ) )
    }

    const numbers = [ ...row.matchAll( /(\d+)/g ) ]
    if ( numbers.length === 0 ) {
        numberPositions.push( [] )
    } else {
        numberPositions.push( numbers.map( number => {
            return {
                value: parseInt( number[ 0 ] ),
                yPositions: number[ 0 ].split( '' ).map( ( e, i ) => number.index + i )
            }
        } ) )
    }
}

const adjacentPairProducts = []
for ( const [ xSymbol, symbolPositions ] of symbolPositionsList.entries() ) {
    if ( symbolPositions.length === 0 ) continue

    const upperRow = numberPositions[ xSymbol - 1 ] || []
    const currentRow = numberPositions[ xSymbol ]
    const lowerRow = numberPositions[ xSymbol + 1 ] || []

    for ( const ySymbol of symbolPositions ) {
        const adjacentPair = checkForAdjacentPair( ySymbol, [ upperRow, currentRow, lowerRow ] )
        if ( adjacentPair ) adjacentPairProducts.push( adjacentPair )
    }
}

function checkForAdjacentPair( ySymbol, rows ) {
    const adjacentNumbers = new Set()
    const yPositions = [ ySymbol - 1, ySymbol, ySymbol + 1 ]

    for ( const y of yPositions ) {
        for ( const row of rows ) {
            const matches = row.filter( number => number.yPositions.includes( y ) )
            if ( matches.length ) adjacentNumbers.add( matches[ 0 ].value )
        }
    }

    if ( adjacentNumbers.size === 2 ) return [ ...adjacentNumbers.values() ].reduce( ( a, b ) => a * b )
    return false
}

const sum = adjacentPairProducts.reduce( ( a, b ) => a + b )
console.log( 'sum :>> ', sum ); // 84900879