const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

let rows = data.split( '\r\n' );

const numberPositions = []
const symbolPositions = []

for ( const row of rows ) {
    const symbols = [ ...row.matchAll( /[^0-9.]/g ) ]
    if ( symbols.length === 0 ) {
        symbolPositions.push( [] )
    } else {
        symbolPositions.push( symbols.map( e => e.index ) )
    }

    const numbers = [ ...row.matchAll( /(\d+)/g ) ]
    if ( numbers.length === 0 ) {
        numberPositions.push( [] )
    } else {
        numberPositions.push( numbers.map( e => {
            return {
                yMin: e.index,
                value: parseInt( e[ 0 ] ),
                yMax: e.index + e[ 0 ].length - 1
            }
        } ) )
    }
}

const adjacentToSymbol = []
for ( let x = 0; x < rows.length; x++ ) {
    for ( number of numberPositions[ x ] ) {
        const upperRow = symbolPositions[ x - 1 ] || []
        const currentRow = symbolPositions[ x ]
        const lowerRow = symbolPositions[ x + 1 ] || []

        const isAdjacent = checkForAdjacentSymbols( number.yMin, number.yMax, [ upperRow, currentRow, lowerRow ] )
        if ( isAdjacent ) adjacentToSymbol.push( number.value )
    }
}
function checkForAdjacentSymbols( yMin, yMax, rows ) {
    for ( let y = yMin - 1; y <= yMax + 1; y++ ) {
        if (
            rows[ 0 ].includes( y ) ||
            rows[ 1 ].includes( y ) ||
            rows[ 2 ].includes( y )
        ) return true
    }
    return false
}

const sum = adjacentToSymbol.reduce( ( a, b ) => a * 1 + b * 1 )
console.log( 'sum :>> ', sum ); // 530849