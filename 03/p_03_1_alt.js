const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

let rows = data.split( '\r\n' );

const numberPositions = []
const symbolPositions = []

for ( const row of rows ) {
    const symbols = [ ...row.matchAll( /[^0-9.]/g ) ]
    if ( symbols.length === 0 ) {
        symbolPositions.push( [] )
    } else {
        symbolPositions.push( symbols.map( e => e.index ) )
    }

    const numbers = [ ...row.matchAll( /(\d+)/g ) ]
    if ( numbers.length === 0 ) {
        numberPositions.push( [] )
    } else {
        numberPositions.push( numbers.map( e => {
            return {
                y: e.index,
                value: parseInt( e[ 0 ] ),
                length: e[ 0 ].length
            }
        } ) )
    }
}

const adjacentToSymbol = []
for ( let x = 0; x < rows.length; x++ ) {
    for ( number of numberPositions[ x ] ) {
        const lowerRowSymbols = symbolPositions[ x - 1 ] || []
        const currRowSymbols = symbolPositions[ x ]
        const upperRowSymbols = symbolPositions[ x + 1 ] || []

        let isAdjacent = checkForAdjacentSymbols()
        if ( isAdjacent ) adjacentToSymbol.push( number.value )

        function checkForAdjacentSymbols() {
            for ( let j = 0; j < number.length; j++ ) {
                const index = number.y + j
                if (
                    [ index, index - 1, index + 1 ].some( ni => lowerRowSymbols.includes( ni ) ) ||
                    [ index - 1, index + 1 ].some( ni => currRowSymbols.includes( ni ) ) ||
                    [ index, index - 1, index + 1 ].some( ni => upperRowSymbols.includes( ni ) )
                ) {
                    return number.value
                }
            }
            return false
        }
    }
}

const sum = adjacentToSymbol.reduce( ( a, b ) => a * 1 + b * 1 )
console.log( 'sum :>> ', sum ); // 530849