const fs = require( 'fs' );
const data = fs.readFileSync( './data2.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );
const rows = data.split( '\r\n' );

const writtenNumbers = [ 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine' ]

const numbers = rows.map( row => {
    const firstNumber = {
        position: row.length
    }
    const lastNumber = {
        position: -1
    }

    for ( const writtenNumber of writtenNumbers ) {
        let posFirstNumber = row.indexOf( writtenNumber )
        if ( posFirstNumber > -1 && firstNumber.position > posFirstNumber ) {
            firstNumber.value = writtenNumbers.indexOf( writtenNumber ) + 1 * 1
            firstNumber.position = posFirstNumber
        }

        let posLastNumber = row.lastIndexOf( writtenNumber )
        if ( posLastNumber > -1 && lastNumber.position < posLastNumber ) {
            lastNumber.value = writtenNumbers.indexOf( writtenNumber ) + 1 * 1
            lastNumber.position = posLastNumber
        }
    }

    for ( let i = 0; i < 10; i++ ) {
        let posFirstNumber = row.indexOf( i )
        if ( posFirstNumber > -1 && firstNumber.position > posFirstNumber ) {
            firstNumber.value = i
            firstNumber.position = posFirstNumber
        }
        let posLastNumber = row.lastIndexOf( i )
        if ( posLastNumber > -1 && lastNumber.position < posLastNumber ) {
            lastNumber.value = i
            lastNumber.position = posLastNumber
        }
    }

    return Number( `${firstNumber.value}${lastNumber.value}` )
} )

const sum = numbers.reduce( ( prev, curr ) => curr + prev );

console.log( 'sum :>> ', sum );
