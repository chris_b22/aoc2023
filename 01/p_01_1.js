const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );
const rows = data.split( '\r\n' );

const numbers = rows.map( e => {
    let nr = e.replace( /[^0-9]/g, '' );
    if ( nr.length < 2 ) nr = nr + nr;
    return Number( nr.at( 0 ) + nr.at( -1 ) )
} )

const sum = numbers.reduce( ( prev, curr ) => curr + prev );

console.log( 'sum :>> ', sum );