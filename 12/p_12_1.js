const fs = require( 'fs' );
const data = fs.readFileSync( './data-test.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let springs = data.split( '\r\n' ).map( e => {
    e = e.split( ' ' )
    return { pattern: e[ 0 ], groupSizes: e[ 1 ].split( ',' ).map( e => Number( e ) ), arrangements: [] }
} )

console.log( 'springs :>> ', springs );



// for ( const row of springs ) {
let row = springs[ 5 ]
// let line = row.pattern
let line = '.??..??...?##.'
line = line.replace( /\.{2,}/g, '.' )
if ( line.startsWith( '.' ) ) line = line.slice( 1 )

for ( const groupSize of row.groupSizes ) {
    let field = ''
    for ( const char of row.pattern ) {
        line = line.slice( 1 )
        if ( char === '?' ) {
            field = field + '?'
            continue
        }

        if ( char === '#' ) {
            field = field + '#'
            if ( field.match( /#/g ).length === groupSize ) {
                line = line.slice( 1 ) // next must be .
                break
            }
            continue
        }
        if ( char === '.' ) break
    }
    console.log( 'field :>> ', field );
    console.log( 'groupSize :>> ', groupSize );
    let countDefect = field.match( /#/g ).length
    let countUnknown = field.match( /?/g ).length


}
// }



console.log( 'area :>> ', field );


// const nextPosition = pattern.shift()