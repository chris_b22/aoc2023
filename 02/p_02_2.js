const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

let games = data.split( '\r\n' ).map( e => e.split( ': ' )[ 1 ] ).map( e => {
    e = e.split( '; ' )
    e = e.map( e => {
        e = e.split( ', ' )
        e = e.map( e => e.split( ' ' ) )
        return e
    } )
    return e
} )

const powerList = []

for ( const game of games ) {
    const cubeCount = {
        red: 0,
        green: 0,
        blue: 0
    }

    for ( const reveal of game ) {
        for ( const cube of reveal ) {
            if ( Number( cube[ 0 ] ) > cubeCount[ cube[ 1 ] ] ) {
                cubeCount[ cube[ 1 ] ] = cube[ 0 ]
            }
        }
    }

    const power = Object.values( cubeCount ).reduce( ( a, b ) => a * b )
    powerList.push( power )
}

const sum = powerList.reduce( ( a, b ) => a + b )
console.log( 'sum :>> ', sum );
