const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

let games = data.split( '\r\n' ).map( e => e.split( ': ' )[ 1 ] ).map( e => {
    e = e.split( '; ' )
    e = e.map( e => {
        e = e.split( ', ' )
        e = e.map( e => e.split( ' ' ) )
        return e
    } )
    return e
} )

const possibleGames = []
const limits = {
    red: 12,
    green: 13,
    blue: 14
}

let gameCount = 0

for ( const game of games ) {
    gameCount++
    if ( checkPossible( game ) ) {
        possibleGames.push( gameCount )
    }
}

function checkPossible( game ) {
    for ( const reveal of game ) {
        for ( cube of reveal ) {
            if ( cube[ 0 ] > limits[ cube[ 1 ] ] ) {
                return false
            }
        }
    }
    return true
}

const sum = possibleGames.reduce( ( a, b ) => a + b )

console.log( 'sum :>> ', sum );
