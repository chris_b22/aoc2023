const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.split( '\r\n' );
const seedsOld = rows.shift().replace( 'seeds: ', '' ).split( ' ' ).map( e => parseInt( e ) )
let seeds = []
for ( let i = 0; i < seedsOld.length; i = i + 2 ) {
    seeds.push( [ seedsOld[ i ], seedsOld[ i ] + seedsOld[ i + 1 ] ] )
}
// console.log( 'seeds :>> ', seeds );

const mapsInput = []
for ( const row of rows ) {
    if ( row === '' ) continue
    if ( row.includes( 'map:' ) ) {
        mapsInput.push( [] )
        continue
    }
    mapsInput.at( -1 ).push( row.split( ' ' ).map( e => parseInt( e ) ) )
}
// console.log( 'mapsInput :>> ', mapsInput );

/*
Man vergleicht immer eine Seed range mit einer Line des Mappings (map range).
- Seed und map überschneiden sich z.b.: S:1-3 M:2-5
- Seed und map überschneiden sich nicht. S:1-3 M:5-8
- Seed ist innerhalb map S:3-4 M:1-6

Es ist egal ob Seed über mehrere map Bereiche geht da nur überschneidungen des aktuellen map range Relevant sind.
In den weiteren Schleifendurchgängen werden möglicherweise weitere map ranges innerhalb der seed range gefunden.
*/
// const newSeeds = []
for ( const map of mapsInput ) {
    const newSeeds = []
    while ( seeds.length >= 1 ) {
        // console.log( seeds );
        const [ startSeed, endSeed ] = seeds.pop()

        let found = false
        for ( const [ change, startChange, changeLength ] of map.values() ) { // a , b,  c
            // overlap = wo Seed und map range sich überschneiden
            const overlapStart = Math.max( startSeed, startChange )
            const overlapEnd = Math.min( endSeed, startChange + changeLength )

            if ( overlapStart < overlapEnd ) {  // nur bei einer überschneidung erfüllt
                newSeeds.push( [ overlapStart - startChange + change, overlapEnd - startChange + change ] ) // a - b = die Änderung bei dest zb.: (50 98 2) 50 bei 98 statt 98 => Änderung -48
                if ( overlapStart > startSeed ) {
                    seeds.push( [ startSeed, overlapStart ] )
                }
                if ( overlapEnd < endSeed ) {
                    seeds.push( [ overlapEnd, endSeed ] )
                }
                found = true
                break
            }
        }
        if ( found === false ) {
            newSeeds.push( [ startSeed, endSeed ] )
        }
    }
    seeds = newSeeds
}
// console.log( 'see :>> ', seeds );
console.log( 'min :>> ', Math.min( ...seeds.map( e => e[ 0 ] ) ) );