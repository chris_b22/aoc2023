const fs = require( 'fs' );
const data = fs.readFileSync( './data-test.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.split( '\r\n' );
const seeds = rows.shift().replace( 'seeds: ', '' ).split( ' ' ).map( e => [ parseInt( e ) ] )
// console.log( 'seeds :>> ', seeds );

const mapsInput = []
for ( const row of rows ) {
    if ( row === '' ) continue
    if ( row.includes( 'map:' ) ) {
        mapsInput.push( [] )
        continue
    }
    mapsInput.at( -1 ).push( row.split( ' ' ).map( e => parseInt( e ) ) )
}
// console.log( 'mapsInput :>> ', mapsInput );

for ( const seedNrs of seeds ) { // Schleife für jeden Seed
    for ( const map of mapsInput ) { // Schleife für jede map
        let seedNrOld = seedNrs.at( -1 )
        let seedNrNew = seedNrs.at( -1 )
        for ( const [ a, b, c ] of map.values() ) { // Schleife für jede line in der map
            if ( b <= seedNrOld && seedNrOld < b + c ) { // nur wenn seed im Bereich der Änderung 
                seedNrNew = a - b + seedNrOld // a - b = die Änderung bei dest zb.: (50 98 2) 50 bei 98 statt 98 => Änderung -48
                break
            }
        }
        seedNrs.push( seedNrNew )
    }
}
// console.log( 'seeds :>> ', seeds );
console.log( 'min :>> ', Math.min( ...seeds.map( e => e.at( -1 ) ) ) );