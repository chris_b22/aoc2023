const fs = require( 'fs' );
const data = fs.readFileSync( './data-test.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.split( '\r\n' );
// console.log( 'rows :>> ', rows );
const seeds = rows.shift().replace( 'seeds: ', '' ).split( ' ' ).map( e => [ parseInt( e ) ] )

const mapsInput = {}
let mapName = ''
for ( const row of rows ) {
    if ( row === '' ) continue
    if ( row.includes( 'map:' ) ) {
        mapName = row.replace( ' map:', '' ).split( '-' )[ 2 ]
        mapsInput[ mapName ] = []
        continue
    }
    mapsInput[ mapName ].push( row.split( ' ' ).map( e => parseInt( e ) ) )
}

const mapsChanges = {}
for ( const [ mapName, lines ] of Object.entries( mapsInput ) ) {
    mapsChanges[ mapName ] = lines.map( line => {
        return {
            changeStart: line[ 1 ],
            changeEnd: line[ 1 ] + line[ 2 ] - 1,
            change: line[ 0 ] - line[ 1 ]
        }
    } )
}
console.log( 'mapsChanges :>> ', mapsChanges );

for ( const seedNrs of seeds ) {
    for ( const map of Object.values( mapsChanges ) ) {
        let seedNrOld = seedNrs.at( -1 )
        let seedNrNew = seedNrs.at( -1 )
        for ( const nrChange of map ) {
            // wenn seed im Bereich führe Änderung durch
            if ( nrChange.changeStart <= seedNrOld && nrChange.changeEnd >= seedNrOld ) seedNrNew = seedNrOld + nrChange.change
        }
        seedNrs.push( seedNrNew )
    }
}
console.log( 'seeds :>> ', seeds );

const locationNrMin = seeds.map( e => e.at( -1 ) ).reduce( ( a, b ) => Math.min( a, b ) )
console.log( 'locationNrMin :>> ', locationNrMin );