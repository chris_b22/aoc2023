const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.replace( /[\(\) ]/g, '' ).split( '\r\n' ).filter( e => e !== '' );
const moves = rows.shift().split( '' )
let paths = rows.map( e => e.split( '=' ) ).map( e => {
    const LR = e[ 1 ].split( ',' )
    return [ e[ 0 ], { L: LR[ 0 ], R: LR[ 1 ] } ]
} )
paths = Object.fromEntries( paths )

let position = paths.AAA
let steps = 0

while ( true ) {
    const next = position[ moves[ steps++ % moves.length ] ] // !!!
    if ( next === 'ZZZ' ) break
    position = paths[ next ]
}

console.log( 'steps :>> ', steps );

// makeMove( 0, 0, 0 )