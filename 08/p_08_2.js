const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

let rows = data.replace( /[\(\) ]/g, '' ).split( '\r\n' ).filter( e => e !== '' );
const moves = rows.shift().split( '' )
const pathsArray = rows.map( e => e.split( '=' ) ).map( e => {
    const LR = e[ 1 ].split( ',' )
    return [ e[ 0 ], { L: LR[ 0 ], R: LR[ 1 ] } ]
} )
const pathsObj = Object.fromEntries( pathsArray )
const startPositionNames = pathsArray.filter( e => e[ 0 ].endsWith( 'A' ) ).map( e => e[ 0 ] )
const stepCounters = []

for ( const name of startPositionNames ) {
    followPath( pathsObj[ name ] )
}

function followPath( position ) {
    let steps = 0
    while ( true ) {
        const next = position[ moves[ steps++ % moves.length ] ] // !!!
        if ( next.endsWith( 'Z' ) ) {
            stepCounters.push( steps )
            break
        }
        position = pathsObj[ next ]
    }
}

function lcm( ...arr ) {
    const gcd = ( x, y ) => ( !y ? x : gcd( y, x % y ) );
    const _lcm = ( x, y ) => ( x * y ) / gcd( x, y );
    return [ ...arr ].reduce( ( a, b ) => _lcm( a, b ) );
};

// console.log( 'stepCounters :>> ', stepCounters );
console.log( 'lcm( stepCounters ) :>> ', lcm( ...stepCounters ) )