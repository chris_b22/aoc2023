const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

const histories = data.split( '\r\n' ).map( e => [ e.split( ' ' ).map( e => parseInt( e ) ) ] )

let history = null
let sum = 0

for ( let i = 0; i < histories.length; i++ ) {
    history = histories[ i ]
    createNextRows()
    createNextNumbers()
    sum = sum + history[ 0 ][ 0 ]
}

function createNextRows( row = 0 ) {
    let next = []
    for ( let i = 0; i < history[ row ].length - 1; i++ ) {
        diff = history[ row ][ i + 1 ] - history[ row ][ i ]
        next.push( diff )
    }
    history.push( next )
    if ( next.some( e => e !== 0 ) ) {
        return createNextRows( row + 1 )
    }
    return
}

function createNextNumbers() {
    history.at( -1 ).unshift( 0 )
    for ( let r = history.length - 1; r > 0; r-- ) {
        history[ r - 1 ].unshift( history[ r - 1 ][ 0 ] - history[ r ][ 0 ] )
    }
}

console.log( 'sum :>> ', sum );