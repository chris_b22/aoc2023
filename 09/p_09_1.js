const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

const histories = data.split( '\r\n' ).map( e => [ e.split( ' ' ).map( e => parseInt( e ) ) ] )

let history = null
let sum = 0

for ( let i = 0; i < histories.length; i++ ) {
    history = histories[ i ]
    createNextRows()
    // createNextNumbers()
    const nextNumber = history.map( e => e.at( -1 ) ).reduce( ( a, b ) => a + b )
    sum = sum + nextNumber
}

function createNextRows( row = 0 ) {
    let next = []
    for ( let i = 0; i < history[ row ].length - 1; i++ ) {
        diff = history[ row ][ i + 1 ] - history[ row ][ i ]
        next.push( diff )
    }
    history.push( next )
    if ( next.some( e => e !== 0 ) ) {
        return createNextRows( row + 1 )
    }
    return
}

// function createNextNumbers() {
//     for ( let r = history.length - 1; r > 0; r-- ) {
//         history[ r - 1 ].push( history[ r ].at( -1 ) + history[ r - 1 ].at( -1 ) )
//     }
// }

console.log( 'sum :>> ', sum );

// const sum = histories.map( e => e[ 0 ].at( -1 ) ).reduce( ( a, b ) => a + b )
