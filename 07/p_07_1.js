const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

const labels = [ '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A' ]
const hands = data.split( '\r\n' ).map( e => {
    const hand = e.split( ' ' )
    let values = hand[ 0 ].split( '' ).map( l => {
        let i = labels.indexOf( l )
        if ( i < 10 ) i = '0' + i
        return i
    } ).join( '-' )
    return [ hand[ 0 ], parseInt( hand[ 1 ] ), hand[ 0 ].split( '' ).sort().join( '' ), values ]
} )
// console.log( 'hands :>> ', hands );

const cardsSorted = {
    distinct: [],
    onePair: [],
    twoPairs: [],
    three: [],
    fullHouse: [],
    four: [],
    five: []
}
const patternsList = {
    distinct: '1-1-1-1-1',
    onePair: '2-1-1-1',
    twoPairs: '2-2-1',
    three: '3-1-1',
    fullHouse: '3-2',
    four: '4-1',
    five: '5'
}

for ( let [ hand, bid, handSorted, points ] of hands.values() ) {
    const labelsOnHand = new Set( hand.split( '' ) )
    const matches = []

    for ( const label of labelsOnHand ) {
        const duplicates = [ 5, 4, 3, 2, 1 ].map( nr => [ nr, new RegExp( `${label}{${nr}}`, 'g' ) ] )
        for ( const nr of duplicates ) {
            if ( handSorted.match( nr[ 1 ] ) ) {
                matches.push( nr[ 0 ] )
                break
            }
        }
    }

    const currentPattern = matches.sort( ( a, b ) => b - a ).join( '-' )
    for ( const [ name, pattern ] of Object.entries( patternsList ) ) {
        if ( pattern === currentPattern ) {
            cardsSorted[ name ].push( [ hand, bid, handSorted, points ] )
            notFound = false
        }
    }
}

for ( const [ pattern, cards ] of Object.entries( cardsSorted ) ) {
    cardsSorted[ pattern ] = cards.sort( ( a, b ) => a[ 3 ] < b[ 3 ] ? -1 : 1 )
}
// console.log( 'cardsSorted :>> ', cardsSorted );

const total = Object.values( cardsSorted ).flat().reduce( ( a, b, i ) => a = a + b[ 1 ] * ( i + 1 ), 0 )
console.log( 'total :>> ', total );