const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

const rows = data.split( '\r\n' )
const matrix = rows.map( e => e.split( '' ) )
const cols = matrix[ 0 ].map( ( e, i ) => matrix.map( row => row[ i ] ) );

const emptyCols = cols.reduce( ( acc, curr, i ) => { if ( !curr.includes( '#' ) ) acc.push( i ); return acc }, [] )
const emptyRows = rows.reduce( ( acc, curr, i ) => { if ( !curr.includes( '#' ) ) acc.push( i ); return acc }, [] )

const galaxyMap = new Map()

let corrX = 0
let nr = 1
for ( let x = 0; x < matrix.length; x++ ) {
    if ( emptyRows[ 0 ] === x ) {
        corrX = corrX + 999999
        emptyRows.shift()
    }

    let corrY = 0
    let emptyColsIndex = 0
    for ( let y = 0; y < matrix[ 0 ].length; y++ ) {
        if ( emptyCols[ emptyColsIndex ] === y ) {
            corrY = corrY + 999999
            emptyColsIndex++
        }
        if ( matrix[ x ][ y ] === '#' ) {
            galaxyMap.set( nr, { x: corrX + x, y: corrY + y } )
            nr++
        }
    }
}

const numbers = Array.from( { length: galaxyMap.size }, ( _, i ) => i + 1 )

const pairs = []
for ( const [ startNr, start ] of galaxyMap.entries() ) {
    numbers.shift()
    for ( const endNr of numbers ) {
        const end = galaxyMap.get( endNr )
        const delta = [ Math.abs( end.x - start.x ), Math.abs( end.y - start.y ) ]
        pairs.push( { pair: startNr + '-' + endNr, distance: delta[ 0 ] + delta[ 1 ] } )
    }
}

const sum = pairs.reduce( ( a, b ) => a + b.distance, 0 )
console.log( 'sum :>> ', sum );