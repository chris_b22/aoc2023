const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

const matrix = data.split( '\r\n' ).map( e => e.split( '' ) )
const start = { distance: 0 } // 62 / 111

matrix.forEach( ( e, i ) => {
    if ( e.indexOf( 'S' ) > -1 ) { start.x = i; start.y = e.indexOf( 'S' ) }
} )

const tiles = {
    'S': [ 'north', 'west' ],
    // 'S': [ 'south', 'east' ], //start for test data
    '|': [ 'north', 'south' ],
    '-': [ 'east', 'west' ],
    'L': [ 'north', 'east' ],
    'J': [ 'north', 'west' ],
    '7': [ 'south', 'west' ],
    'F': [ 'south', 'east' ],
}
const dirs = {
    north: [ -1, 0 ],
    east: [ 0, 1 ],
    south: [ 1, 0 ],
    west: [ 0, -1 ]
}

let maxDistance = 0
const visited = []
const positions = [ start ]

while ( positions.length > 0 ) {
    const p = positions.shift()
    visited.push( p.x + '_' + p.y )
    if ( p.distance > maxDistance ) maxDistance = p.distance

    const directions = tiles[ matrix[ p.x ][ p.y ] ]
    for ( const direction of directions ) {
        const [ dirX, dirY ] = dirs[ direction ]
        const nextPos = {
            x: p.x + dirX,
            y: p.y + dirY,
            distance: p.distance + 1
        }
        if ( !visited.includes( nextPos.x + '_' + nextPos.y ) ) {
            positions.push( nextPos )
        }
    }
}
console.log( 'maxDistance :>> ', maxDistance );