const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );

const matrix = data.split( '\r\n' ).map( e => e.split( '' ) )
const start = { distance: 0 } // 62 / 111

matrix.forEach( ( e, i ) => {
    if ( e.indexOf( 'S' ) > -1 ) { start.x = i; start.y = e.indexOf( 'S' ) }
} )
matrix[ start.x ][ start.y ] = 'J'
// matrix[ start.x ][ start.y ] = 'F' // Test data

const tiles = {
    '|': [ 'north', 'south' ],
    '-': [ 'east', 'west' ],
    'L': [ 'north', 'east' ],
    'J': [ 'north', 'west' ],
    '7': [ 'south', 'west' ],
    'F': [ 'south', 'east' ],
}
const dirs = {
    north: [ -1, 0 ],
    east: [ 0, 1 ],
    south: [ 1, 0 ],
    west: [ 0, -1 ]
}

const newMatrix = matrix.map( ( e, i, a ) => '.'.repeat( a[ 0 ].length ).split( '' ) )

const visited = []
const positions = [ start ]

while ( positions.length > 0 ) {
    const p = positions.shift()
    visited.push( p.x + '_' + p.y )
    newMatrix[ p.x ][ p.y ] = matrix[ p.x ][ p.y ]

    const directions = tiles[ matrix[ p.x ][ p.y ] ]
    for ( const direction of directions ) {
        const [ dirX, dirY ] = dirs[ direction ]
        const nextPos = {
            x: p.x + dirX,
            y: p.y + dirY,
            distance: p.distance + 1
        }
        if ( !visited.includes( nextPos.x + '_' + nextPos.y ) ) {
            positions.push( nextPos )
        }
    }
}

const rotatedMatrix = newMatrix[ 0 ].map( ( e, i ) => newMatrix.map( row => row[ i ] ) )

const borders = {
    up: [ '7L', 'FJ' ], // Suchwert gedreht statt Array
    down: [ '7L', 'FJ' ],
    left: [ 'FJ', 'L7' ], // ebenfalls gedreht
    right: [ 'L7', 'FJ' ]
}

for ( const [ x, row ] of newMatrix.entries() ) {
    for ( const [ y, pos ] of row.entries() ) {
        if ( pos !== '.' ) continue
        if ( isInsideLoop( x, y, row, rotatedMatrix[ y ] ) ) {
            newMatrix[ x ][ y ] = 'I'
        } else {
            newMatrix[ x ][ y ] = 'O'
        }
    }
}

function isInsideLoop( x, y, row, col ) {
    const directions = {
        up: col.slice( 0, x ),
        right: row.slice( y + 1 ),
        down: col.slice( x + 1 ),
        left: row.slice( 0, y )
    }

    if ( Object.values( directions ).some( e => e.length === 0 ) ) return false;

    // Wenn in einer Richtung kein Symbol außer . vorhanden ist => dann außerhalb
    const isBesideEdge = () => {
        for ( const dir of Object.values( directions ) ) {
            let includesChar = false
            for ( const char of Object.keys( tiles ) ) {
                if ( dir.includes( char ) ) {
                    includesChar = true
                    break
                }
            }
            if ( includesChar ) continue
            return true
        }
        return false
    }
    if ( isBesideEdge() ) return false

    for ( let [ dir, characters ] of Object.entries( directions ) ) {
        characters = characters.join( '' )
        let straightBorder = ''
        if ( dir === 'left' || dir === 'right' ) {
            characters = characters.replaceAll( '-', '' )
            straightBorder = '|'
        } else {
            characters = characters.replaceAll( '|', '' )
            straightBorder = '-'
        }
        const borderRegex = new RegExp( `(${borders[ dir ][ 0 ]})|(${borders[ dir ][ 1 ]})|(\\${straightBorder})`, 'g' )
        const crossings = characters.match( borderRegex )
        if ( crossings === null ) continue
        if ( crossings.length % 2 !== 0 ) {
            return true
        }
    }
    return false
}

// console.log( 'newMatrix :>> ', newMatrix );
// newMatrix.forEach( e => console.log( e.join( '' ) ) )
const count = newMatrix.reduce( ( a, b ) => a + b.filter( e => e === 'I' ).length, 0 )
console.log( 'count :>> ', count );