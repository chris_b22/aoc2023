const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.split( '\r\n' ).map( e => e.split( ':' )[ 1 ].split( ' ' ).filter( e => e !== '' ).map( e => parseInt( e ) ) )
let table = rows[ 0 ].map( ( e, i ) => [ e, rows[ 1 ][ i ] ] )
// console.log( 'rows :>> ', table );

const results = []

for ( const [ time, distance ] of table.values() ) {
    let wins = 0
    for ( let tButton = 0; tButton <= time; tButton++ ) {
        const tMove = time - tButton
        if ( tButton * tMove > distance ) wins++
    }
    results.push( wins )
}

console.log( 'results :>> ', results.reduce( ( a, b ) => a * b ) );