const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.split( '\r\n' ).map( e => e.split( ': ' )[ 1 ] ).map( e => e.split( ' | ' ) )
const cards = rows.map( numberLists => {
    numberLists.forEach( ( numbers, i, a ) => a[ i ] = numbers.split( ' ' ).map( nr => parseInt( nr ) ).filter( e => !isNaN( e ) ) )
    return {
        winningNrs: numberLists[ 0 ],
        receivedNrs: numberLists[ 1 ],
        points: 0
    }
} )

let points = 0

for ( const card of cards ) {
    let pointsValue = 1
    for ( const winningNr of card.winningNrs ) {
        if ( card.receivedNrs.includes( winningNr ) ) {
            card.points = pointsValue
            pointsValue = pointsValue * 2
        }
    }
    points = points + card.points
}

console.log( 'points :>> ', points );
// console.log( 'cards :>> ', cards );