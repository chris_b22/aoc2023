const fs = require( 'fs' );
const data = fs.readFileSync( './data.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );

let rows = data.split( '\r\n' ).map( e => e.split( ': ' )[ 1 ] ).map( e => e.split( ' | ' ) )
const cards = rows.map( numberLists => {
    numberLists.forEach( ( numbers, i, a ) => a[ i ] = numbers.split( ' ' ).map( nr => parseInt( nr ) ).filter( e => !isNaN( e ) ) )
    return {
        winningNrs: numberLists[ 0 ],
        receivedNrs: numberLists[ 1 ],
        instances: 1
    }
} )

for ( const [ i, card ] of cards.entries() ) {
    let nrCount = 0
    for ( const winningNr of card.winningNrs ) {
        if ( card.receivedNrs.includes( winningNr ) ) nrCount++
    }

    for ( let index = i + 1; index <= i + nrCount; index++ ) {
        cards[ index ].instances = cards[ index ].instances + card.instances
    }
}

const sum = cards.reduce( ( a, b ) => a + b.instances, 0 )

console.log( 'sum :>> ', sum );
// console.log('cards :>> ', cards);